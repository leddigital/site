<?php
class ControllerCommonMenu extends Controller
{
	public function index()
	{
		$this->load->language('common/menu');

		if (isset($this->request->get['route'])) {
			$data['route'] = $this->request->get['route'];
		}

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		//Carregar logo
		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}
		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						//'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'name'  => $child['name'],
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);

				$data['home'] = $this->url->link('common/home');
				$data['wishlist'] = $this->url->link('account/wishlist', '', true);
				$data['logged'] = $this->customer->isLogged();
				$data['account'] = $this->url->link('account/account', '', true);
				$data['register'] = $this->url->link('account/register', '', true);
				$data['login'] = $this->url->link('account/login', '', true);
				$data['order'] = $this->url->link('account/order', '', true);
				$data['transaction'] = $this->url->link('account/transaction', '', true);
				$data['download'] = $this->url->link('account/download', '', true);
				$data['logout'] = $this->url->link('account/logout', '', true);
				$data['shopping_cart'] = $this->url->link('checkout/cart');
				$data['checkout'] = $this->url->link('checkout/checkout', '', true);
				$data['contact'] = $this->url->link('information/contact');
				$data['telephone'] = $this->config->get('config_telephone');

				$data['language'] = $this->load->controller('common/language');
				$data['currency'] = $this->load->controller('common/currency');
				$data['search'] = $this->load->controller('common/search');
				$data['cart'] = $this->load->controller('common/cart');
			}
		}

		return $this->load->view('common/menu', $data);
	}
}
