<?php
// Text
$_['text_information']  = 'Informações';
$_['text_service']      = 'Serviços ao cliente';
$_['text_extra']        = 'Outros serviços';
$_['text_contact']      = 'Entre em contato';
$_['text_return']       = 'Solicitar devolução';
$_['text_sitemap']      = 'Mapa do site';
$_['text_manufacturer'] = 'Produtos por marca';
$_['text_voucher']      = 'Comprar vale presentes';
$_['text_affiliate']    = 'Programa de afiliados';
$_['text_special']      = 'Produtos em promoção';
$_['text_account']      = 'Minha conta';
$_['text_order']        = 'Histórico de pedidos';
$_['text_wishlist']     = 'Lista de desejos';
$_['text_newsletter']   = 'Informativo';
$_['text_powered']      = 'Desenvolvido com tecnologia <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';



// Heading
//$_['heading_title']  = 'Entre em contato';

// Text
$_['text_location']  = 'Nossa localização';
$_['text_store']     = 'Nossas lojas';
$_['text_contact']   = 'Formulário de contato';
$_['text_address']   = 'Endereço';
$_['text_telephone'] = 'Telefone';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Horário de funcionamento';
$_['text_comment']   = 'Comentários';
$_['text_success']   = '<p>Sua dúvida foi enviada para nosso atendimento.</p>';

// Entry
$_['entry_name']     = 'Seu nome';
$_['entry_email']    = 'Seu e-mail';
$_['entry_enquiry']  = 'Mensagem';

// Email
$_['email_subject']  = 'Contato - %s';

// Errors
$_['error_name']     = 'O seu nome deve ter entre 3 e 32 caracteres.';
$_['error_email']    = 'O e-mail não é válido.';
$_['error_enquiry']  = 'A dúvida deve ter entre 10 e 3000 caracteres.';