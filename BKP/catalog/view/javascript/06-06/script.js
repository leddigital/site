
jQuery(document).ready(function() {
    if(jQuery(window).width() <= 425) {

        jQuery(".bloco").removeClass("no-float");

        jQuery(".main-menu-flex").removeClass("main-menu-flex");
        jQuery(".main-menu-wrapper").removeClass("main-menu-wrapper");
    }
});

jQuery(window).resize(function(){
    if(jQuery(window).width() <= 425) {

        jQuery(".bloco").removeClass("no-float");

        jQuery(".main-menu-flex").removeClass("main-menu-flex");
        jQuery(".main-menu-wrapper").removeClass("main-menu-wrapper");
        console.log(jQuery(window).width());
    }
});

jQuery(window).load(function(){
    if(jQuery(window).width() <= 425) {
        jQuery(".main-menu-flex").removeClass("main-menu-flex");
        jQuery(".main-menu-wrapper").removeClass("main-menu-wrapper");
    }
});

jQuery(document).ready(function(){
    jQuery("button.btn-navbar").click(function(){
        if(jQuery(".navbar-collapse").hasClass("in")){
            jQuery("button.btn-navbar").css({ "background-color":"#b6c931","border":"none" });
            jQuery("#menu-button").removeClass("fa-times")
            jQuery("#menu-button").addClass("fa-bars");
        } else {
            jQuery("button.btn-navbar").css({ "background-color":"#225f27","border":"none" });
            jQuery("#menu-button").removeClass("fa-bars");
            jQuery("#menu-button").addClass("fa-times");
        }
    });
});

jQuery(document).ready(function(){
    jQuery(".social-media>ul>li").mouseenter(function(){
        jQuery(this).addClass("animated bounceIn");
    });
    jQuery(".social-media").mouseleave(function(){
        jQuery(".social-media>ul>li").removeClass("animated bounceIn");
    });
});

jQuery(document).ready(function(){
    jQuery("img").addClass("img-responsive");
});