<?php
// HTTP
define('HTTP_SERVER', 'http://herbicat.agencialed.com.br/admin/');
define('HTTP_CATALOG', 'http://herbicat.agencialed.com.br/');

// HTTPS
define('HTTPS_SERVER', 'http://herbicat.agencialed.com.br/admin/');
define('HTTPS_CATALOG', 'http://herbicat.agencialed.com.br/');

// DIR
define('DIR_APPLICATION', '/home2/agenci62/herbicat.agencialed.com.br/admin/');
define('DIR_SYSTEM', '/home2/agenci62/herbicat.agencialed.com.br/system/');
define('DIR_IMAGE', '/home2/agenci62/herbicat.agencialed.com.br/image/');
define('DIR_STORAGE', '/home2/agenci62/herbicat.agencialed.com.br/storage/');
define('DIR_CATALOG', '/home2/agenci62/herbicat.agencialed.com.br/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'agenci62_nv_herb');
define('DB_PASSWORD', 'T@~]BFU&bWjp');
define('DB_DATABASE', 'agenci62_novo_herbicat');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
