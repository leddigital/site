<?php
class ControllerCustompagesSinglepost extends Controller
{
	public function index()
	{

		$this->load->model('catalog/singlepost');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');


		$query = $this->model_catalog_singlepost->getSinglePost($_GET['id']);
		$popular_products = $this->model_catalog_product->getPopularProducts(3);
		$latest_posts = $this->model_catalog_singlepost->getLatestPosts($_GET['id']);

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}


		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['search'] = $this->load->controller('common/search');
		$data['base'] = $server;

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('<i class="fa fa-home" aria-hidden="true"></i>'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Notícias e Artigos',
			'href' => $this->url->link('custompages/news')
		);

		$data['breadcrumbs'][] = array(
			'text' => $query->row['title'],
			'href' => $this->url->link('javasript:;')
		);

		if ($query->row['image']) {
			$image = $this->model_tool_image->resize($query->row['image'], 1280, 720);
		} else {
			$image = $this->model_tool_image->resize('placeholder.png', 500, 250);
		}

		$data['post'] = array(
			'title' 				=> 			$query->row['title'],
			'date_published'		=>			$query->row['date_published'],
			'image' 				=> 			$image,
			'short_description' 	=> 			$query->row['short_description'],
			'description'			=>			$query->row['description'],
			'tag' 					=> 			$query->row['tag'],		
		);


	

		foreach ($popular_products as $result) {
			
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], 500, 300);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', 500, 250);
			}

			$data['popular_products'][] = array(
				'name' 					=> 			$result['name'],
				'image' 				=> 			$image,
				'price' 				=> 			$result['price'],
				'description' 			=> 			$result['description'],		
				'product_id'			=>			$result['product_id'],
				'meta_title'			=>			$result['meta_title']
			);
		}

		foreach ($latest_posts as $result) {
			
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], 500, 250);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', 500, 250);
			}
			
			$data['latest_posts'][] = array (
				'post_id'				=>			$result['post_id'],
				'image' 				=> 			$image,
				'title'					=>			$result['title'],
				'viewed'				=>			$result['viewed'],
				'short_description'		=>			$result['short_description']
			);
		}

		$this->response->setOutput($this->load->view('custompages/singlepost', $data));
	}
}
