<?php
class ModelCatalogNews extends Model {

	public function getPaginationNews(){

		$post_number = $this->db->query("SELECT count(post_id) AS num_post FROM oc_bm_post;");
		return $post_number;
	}

	public function getNews($page) {

		$posts = $this->getPaginationNews();
		$post_number = $posts->row['num_post'];
			
		$limit = 6;
		$offset = (6*$page)-$limit;
		
		$sql = "SELECT * FROM oc_bm_post p 
		INNER JOIN oc_bm_post_description pc
		ON p.post_id = pc.post_id
		LIMIT ".$limit." OFFSET ".$offset;

		$query = $this->db->query($sql);

		return $query->rows;
	}	
}
