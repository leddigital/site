<?php
class ModelCatalogSinglepost extends Model {

	public function getSinglePost($post_id) {
		$query = $this->db->query("SELECT * FROM oc_bm_post p 
		INNER JOIN oc_bm_post_description pc
		ON p.post_id = pc.post_id
		WHERE p.post_id AND pc.post_id = ".$post_id." LIMIT 1");

		return $query;
	}	

	public function getLatestPosts($actual_id = ""){
		

		if ($actual_id != "") {
			$sql = "SELECT ps.post_id, ps.image, ps.viewed, ps.date_published, p.title, p.short_description
			FROM oc_bm_post AS ps
			INNER JOIN oc_bm_post_description AS p
			ON ps.post_id = p.post_id
			WHERE ps.post_id <> ".$actual_id."
			ORDER BY ps.date_published DESC
			LIMIT 3;";
		} else {
			$sql = "SELECT ps.post_id, ps.image, ps.viewed, ps.date_published, p.title, p.short_description
			FROM oc_bm_post AS ps
			INNER JOIN oc_bm_post_description AS p
			ON ps.post_id = p.post_id
			ORDER BY ps.date_published DESC
			LIMIT 3;";
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
}
