<?php
// HTTP
define('HTTP_SERVER', 'http://local.herbicat.com.br/');

// HTTPS
define('HTTPS_SERVER', 'http://local.herbicat.com.br/');

// DIR
define('DIR_APPLICATION', '/mnt/projects/www/herbicat.com.br/catalog/');
define('DIR_SYSTEM', '/mnt/projects/www/herbicat.com.br/system/');
define('DIR_IMAGE', '/mnt/projects/www/herbicat.com.br/image/');
define('DIR_STORAGE', '/mnt/projects/www/storage/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'agencialed');
define('DB_DATABASE', 'herbicat_loja');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');